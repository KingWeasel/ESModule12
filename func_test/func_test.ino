//memory test definitions//////////////////////
#define FLASH_BEGIN (char *)0x08000000
#define FLASH_END (char *)0x0801FFFF
#define SRAM_BEGIN (char *)0x20000000
#define SRAM_END (char *)0x20004FFF

struct bounds
{
  char *start_addr;
  char *stop_addr;
};
///////////////////////////////////////////////

//PWM/ADC test definitions////////////////////
#define PWM_PIN 7
#define ADC_PIN 15
#define MAX_DUTY_CYCLE 65535
#define MAX_ADC_VAL 4095
#define NUM_SAMPLES 1000
#define PWM_LSB2VOLT 0.000050354//Volts per bit
#define ADC_LSB2VOLT 0.000805664//Volts per bit
///////////////////////////////////////////////

//timer interrupt test definitions/////////////
#define PERIOD 1000000    // in microseconds, 1Hz toggles

HardwareTimer timer(3);
double t0 = 0, dt = 0;
int counter = 0;
///////////////////////////////////////////////

//external interrupt test definitions//////////
#define BUTTON_IN_PIN 0

bool buttonPressed = false;
///////////////////////////////////////////////

//text input//////////////////////////////////
#define STRING_MAX 200

String inputString = "";// a String to hold incoming data
////////////////////////////////////////////////////////////////

void setup()
{
  // initialize serial
  Serial.begin(115200);
  while (!Serial);
  Serial.flush();
  Serial.println("Starting...");
  Serial.println();

  // reserve STRING_MAX bytes for the inputString
  inputString.reserve(STRING_MAX);
}

void loop()
{
  while (Serial.available() > 0)
    inputString.concat((char)Serial.read());

  if (inputString.length() > 0)
  {
    process_string(inputString);
    inputString.remove(0);
  }
}

void process_string(String inputString)
{
  String int_holder = "";
  int_holder.reserve(STRING_MAX);

  inputString.trim();

  if (inputString.equalsIgnoreCase("help"))
    print_help();
  else if (inputString.equalsIgnoreCase("m"))
    do_mem_test();
  else if (inputString.equalsIgnoreCase("a"))
    do_adc_test();
  else if (inputString.equalsIgnoreCase("t"))
    do_timer_interrupt_test();
  else if (inputString.equalsIgnoreCase("b"))
    do_button_interrupt_test();
  else if (inputString.equalsIgnoreCase("x"))
    do_all_tests();
  else
  {
    Serial.println("\nUnrecognized input. Printing help instead...");
    print_help();
  }
}

void print_help()
{
  Serial.println("\n|Help Menu|");
  Serial.println("----------------");
  Serial.println("Print help - help");
  Serial.println("Run memory test - m");
  Serial.println("Run adc test - a");
  Serial.println("Run timer interrupt test - t");
  Serial.println("Run button interrupt test - b");
  Serial.println("Run all tests - x");
  Serial.println();
}

void do_mem_test()
{
  bounds flash_bounds = {FLASH_BEGIN, FLASH_END};
  bounds sram_bounds = {SRAM_BEGIN, SRAM_END};

  Serial.println("Beginning memory test...");
  Serial.println("----------------");
  Serial.println();

  Serial.println("Beginning flash boundary scan...");
  boundary_scan(&flash_bounds);
  delay(10);
  Serial.println("Beginning sram boundary scan...");
  boundary_scan(&sram_bounds);
  delay(10);
  Serial.println("Beginning flash corruption scan...");
  corruption_scan(&flash_bounds);
  delay(10);
  Serial.println("Beginning SRAM corruption scan...");
  corruption_scan(&sram_bounds);
  delay(10);
  Serial.println();
}

void do_adc_test()
{
  pinMode(PWM_PIN, PWM);
  int accumulator, duty_cycle, average = 0;
  double pwm_volts, adc_volts;

  Serial.println("Beginning ADC/DAC test...");
  Serial.println("----------------");
  Serial.println();

  for (int i = 0; i < 10; i++)
  {
    accumulator = 0;
    duty_cycle = MAX_DUTY_CYCLE / pow(2, i);
    pwmWrite(PWM_PIN, duty_cycle);
    delay(50);
    for (int j = 0; j < NUM_SAMPLES; j++)
    {
      accumulator += analogRead(ADC_PIN);
      delay(1);
    }

    average = accumulator / NUM_SAMPLES;

    pwm_volts = duty_cycle * PWM_LSB2VOLT;
    adc_volts = average * ADC_LSB2VOLT;

    Serial.print("DAC put out ");
    Serial.print(pwm_volts, 5);
    Serial.println(" VDC");
    Serial.print("ADC read ");
    Serial.print(adc_volts, 5);
    Serial.println(" VDC");
    Serial.print("Absolute difference is ");
    Serial.print(abs(pwm_volts - adc_volts), 5);
    Serial.println(" VDC");
    Serial.println();
  }
}

void do_timer_interrupt_test()
{
  Serial.println("Beginning timer interrupt test...");
  Serial.println("----------------\n");
  // Pause the timer while we're configuring it
  timer.pause();

  // Set up period
  timer.setPeriod(PERIOD); // in microseconds

  // Set up an interrupt on channel 1
  //also straight from example
  timer.setMode(1, TIMER_OUTPUT_COMPARE);
  timer.setCompare(1, 1);  // Interrupt 1 count after each update

  t0 = micros();
  Serial.print("t0: ");
  Serial.println(t0);
  counter = 0;

  timer.attachInterrupt(1, sec_handler);
  // Refresh the timer's count, prescale, and overflow
  timer.refresh();
  // Start the timer counting
  timer.resume();

  while (counter < 10)
    delay (500);

  timer.detachInterrupt(1);
  timer.pause();
  Serial.println();
}

void do_button_interrupt_test()
{
  Serial.println("Beginning button interrupt test...");
  Serial.println("----------------");
  Serial.println();

  buttonPressed = false;

  pinMode(BUTTON_IN_PIN, INPUT);
  attachInterrupt(BUTTON_IN_PIN, switch_state, RISING);

  Serial.println("Press the button within 5 seconds");

  double start_test = millis();

  while (!buttonPressed && millis() - start_test < 5000)
  {
    Serial.println((millis() - start_test) / 1000.0);
    delay(1000);
  }

  if (!buttonPressed)
    Serial.println("External interrupt not detected");
  else
    Serial.println("External interrupt detected");

  buttonPressed = false;
  detachInterrupt(BUTTON_IN_PIN);
  Serial.println();
}

void do_all_tests()
{
  do_mem_test();
  do_adc_test();
  do_timer_interrupt_test();
  do_button_interrupt_test();
}

void boundary_scan(bounds *mem_bounds)
{ //can scan up

  char *mem_loc = mem_bounds->start_addr, *begin_of_end = mem_bounds->stop_addr;
  bool first_found = HIGH;

  while (mem_loc <= mem_bounds->stop_addr)
  {
    if (*mem_loc == 0xFF && first_found)
    { //FF found and start flag hasn't tripped yet
      begin_of_end = mem_loc;
      first_found = LOW;
    }
    else if (*mem_loc != 0xFF)
    {
      first_found = HIGH;
    }

    mem_loc++;
  }

  mem_bounds->stop_addr = --begin_of_end;

  Serial.print("Start addr: 0x");
  Serial.println((int)mem_bounds->start_addr, HEX);
  Serial.print("Stop addr: 0x");
  Serial.println((int)mem_bounds->stop_addr, HEX);
  Serial.print("Memory Range: ");
  Serial.print(mem_bounds->stop_addr - mem_bounds->start_addr);
  Serial.println(" bytes");
  delay(10);
  return;
}

void corruption_scan(bounds *mem_bounds)
{
  char *mem_loc = mem_bounds->start_addr;
  int fault_counter = 0, mem_val;

  while (mem_loc <= mem_bounds->stop_addr)
  {
    if (*mem_loc == 0xFF)
      fault_counter++;

    mem_loc++;
  }

  Serial.print("0xFFs found: ");
  Serial.println(fault_counter);
  delay(10);
  return;
}

void sec_handler()
{
  dt = micros() - t0;
  Serial.print("dt since t0: ");
  Serial.print(dt);
  Serial.println(" usec");
  counter++;
}

void switch_state()
{
  buttonPressed = true;
}
